<?php

namespace App\Http\Controllers;

use App\Http\Requests\SanPham\CreateSanPhamRequest;
use App\Http\Requests\SanPham\UpdateSanPhamRequest;
use App\Models\SanPham;
use App\Models\DanhMuc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Str;

class SanPhamController extends Controller
{

    // private $groupRule;

    // public function __construct()
    // {
    //     $this->groupRule = app(GroupRuleController::class);
    // }

    public function index()
    {
        // if($this->groupRule->checkRule(3) == false){
        //     toastr()->error('Bạn không đủ quyền truy cập chức năng này!');
        //     return redirect('/admin/');
        // }
        $listSanPham = SanPham::all();

        return view('admin.page.san_pham.index', compact('listSanPham'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // if($this->groupRule->checkRule(1) == false){
        //     toastr()->error('Bạn không đủ quyền truy cập chức năng này!');
        //     return redirect('/admin/');
        // }

        $danh_muc = DanhMuc::where('is_open', 1)->get();

        return view('admin.page.san_pham.create', compact('danh_muc'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSanPhamRequest $request)
    {
        $data = $request->all();
        $data['so_luong'] = 0;
        SanPham::create($data);

        return response()->json(['status' => true]);
    }

    public function show(SanPham $sanPham)
    {
        //
    }

    public function edit($id)
    {
        // B1: Tìm sản phẩm dựa vào id mà nó muốn tìm
        $san_pham = SanPham::find($id);
        // Kiểm tra tìm thấy hay không?
        if($san_pham) {
            $danh_muc = DanhMuc::where('is_open', 1)->get();
            return view('admin.page.san_pham.edit', compact('san_pham', 'danh_muc'));
        } else {
            toastr()->error('Sản phẩm không tồn tại!');
            return redirect('/admin/san-pham/index');
        }
    }


    public function update(UpdateSanPhamRequest $request)
    {
        $san_pham = SanPham::find($request->id);

        $data = $request->all(); 


        $san_pham->update($data);

        return response()->json(['status' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SanPham  $sanPham
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $san_pham = SanPham::find($id);
        if($san_pham) {
            $san_pham->delete();
            return response()->json(['status' => true]);
        } else {
            return response()->json(['status' => false]);
        }
    }

    public function checkSlug(Request $request)
    {
        if($request->slug_san_pham) {
            $data = SanPham::where('slug_san_pham', $request->slug_san_pham)
                            ->first();
        } else {
            return response()->json(['status' => true]); // true thì báo không được

        }
        if($data) {
            return response()->json(['status' => true]); // true thì báo không được
        } else {
            return response()->json(['status' => false]);
        }
    }

    public function list()
    {
        $san_pham = SanPham::where('is_open', 1)
                           ->orderByDesc('so_luong')
                           ->take(10)
                           ->get();
        return response()->json(['data' => $san_pham]);
    }

    public function search(Request $request)
    {
        $san_pham = SanPham::where('ten_san_pham', 'like', '%' . $request->name . '%')
                            ->orderByDesc('so_luong')
                            ->take(10)
                            ->get();
        return response()->json(['data' => $san_pham]);
    }

    // public function viewSanPhamHomePage($link)
    // {
    //     while(strpos($link, 'post')){
    //         $pos = strpos($link, 'post');
    //         $link = Str::substr($link, $pos + 4, strlen($link) - ($pos + 4));
    //     }
    //     $id = $link;
    //     $san_pham = SanPham::find($id);
    //     if($san_pham) {
    //         $san_pham_lien_quan = SanPham::where('danh_muc_id', $san_pham->danh_muc_id)->get();
    //         return view('client.pages.product.detail', compact('san_pham', 'san_pham_lien_quan'));
    //     } else {
    //         toastr()->error("Sản phẩm không tồn tại!");
    //         return redirect('/');
    //     }
    // }
}
