<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kho extends Model
{
    use HasFactory;

    protected $table =  'khos';
    protected $fillable = [
        'id_san_pham',
        'ten_san_pham',
        'so_luong_nhap',
        'don_gia_nhap',
        'hoa_don_nhap',
    ];
}
