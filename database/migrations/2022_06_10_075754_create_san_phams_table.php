<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {  
        Schema::create('san_phams', function (Blueprint $table) {
            $table->id();
            $table->string('ten_san_pham');
            $table->string('slug_san_pham');
            $table->integer('is_open');
            $table->integer('gia_ban');
            $table->integer('gia_khuyen_mai');
            $table->text('mo_ta_ngan');
            $table->longText('mo_ta_chi_tiet');
            $table->integer('so_luong');
            $table->string('anh_dai_dien');
            $table->string('danh_muc_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('san_phams');
    }
};
