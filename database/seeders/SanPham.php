<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SanPham extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('san_phams')->delete();

        DB::table('san_phams')->truncate();

        DB::table('san_phams')->insert([
            [
                'ma_san_pham'  => 'CHONUOCNGOAI',
                'ten_san_pham' => 'Chó chăn gia súc Flanders' ,
                'slug_san_pham' => 'cho-chan-gia-suc-flanders' ,
                'so_luong' => 5,
                'don_gia_ban' => 40000000,
                'don_gia_khuyen_mai' => 30000000,
                'hinh_anh' => 'https://galaxydidong.vn/wp-content/uploads/2021/10/iphone-13-pro-max-gold-1-600x600.jpg',
                'mo_ta_ngan' => 'Chúng có thể được tạo ra từ việc lai giữa loài Griffon và chó Beauceron. Loài Bouviêr des Flandres, xuất xứ từ loài chó chăn gia súc, được sử dụng rộng rãi như chó cứu hộ và vận chuyển thư trong suốt Thế Chiến Thứ I. Kết quả là sự hiện diện của nó trong suốt Thế chiến và sự hủy diệt gần như toàn bộ tỉnh Flandres, loài này gần như bị tiêu diệt hoàn toàn.                ',
                'mo_ta_chi_tiet' => 'Chiều dài thận từ đỉnh vai đến đỉnh của mông nên cân đối xấp xỉ với chiều dài của chó từ hai vai đến mặt đất, tạo nên cho chó một bộ khung cân đối. Hai chân trước thì cơ bắp và thẳng tắp. Ngực rộng và sâu. Dòng chó Bỉ thường có khuynh hướng có phần nhỏ hơn và nhẹ hơn dòng chó Hà Lan. Chiều cao, cân năng: Chó đực cao 58 đến 71 cm, nặng 34 đến 41 cân. Chó cái cao từ 56 đến 59 cm, nặng 27 đến 36 cân.                ',
                'danh_muc_id' => 2,
                'is_open' => 1,
            ],
            [
                'ma_san_pham'  => 'CHOVN',
                'ten_san_pham' => 'Cho Co' ,
                'slug_san_pham' => 'cho-co' ,
                'so_luong' => 14,
                'don_gia_ban' => 16990000,
                'don_gia_khuyen_mai' => 14490000,
                'hinh_anh' => 'https://galaxydidong.vn/wp-content/uploads/2019/12/iPhone-11-pro-600x600.jpg',
                'mo_ta_ngan' => 'Chó Nhật (tiếng Nhật: 狆, chin) là giống chó của hoàng gia Nhật. Giống chó này thường được nuôi làm cảnh hoặc để bầu bạn.
                ',
                'mo_ta_chi_tiet' => 'Khi đứng, chó Nhật cao khoảng 20 đến 27 cm (8 – 11 in) tính đến vai. Khối lượng của chúng rất đa dạng, từ 3 lbs cho đến 15 lbs, thông thường trung bình là 7 đến 9 pound. Giống chó phương Đông này có đầu lớn, mắt to, tai mọc lông dài và có khoang hoa văn ở mặt.
                ',
                'danh_muc_id' => 1,
                'is_open' => 1,
            ],
            [
                'ma_san_pham'  => 'IP3',
                'ten_san_pham' => 'Iphone 12 Pro 128gb',
                'slug_san_pham' => 'iphone-12-128gb',
                'so_luong' => 1,
                'don_gia_ban' => 30490000,
                'don_gia_khuyen_mai' => 26990000,
                'hinh_anh' => 'https://www.xtmobile.vn/vnt_upload/product/11_2020/thumbs/600_iphone_12_xanh_la_xtmobile.jpg',
                'mo_ta_ngan' => 'Nâng cao chất lượng ảnh với hệ thống camera được nâng cấp.
                                Chơi game 3D mượt mà với chip A15 Bionic, GPU 5 lõi mới.
                                Trải nghiệm quét 120 Hz, độ sáng 1200 nits.
                                Thao tác cảm ứng thông minh ProMotion và hệ điều hành iOS 15 mới',
                'mo_ta_chi_tiet' => 'Nâng cao chất lượng ảnh với hệ thống camera được nâng cấp.
                                    Chơi game 3D mượt mà với chip A15 Bionic, GPU 5 lõi mới.
                                    Trải nghiệm quét 120 Hz, độ sáng 1200 nits.
                                    Thao tác cảm ứng thông minh ProMotion và hệ điều hành iOS 15 mới',
                'danh_muc_id' => 3,
                'is_open' => 1,
            ],
            [
                'ma_san_pham'  => 'IP3',
                'ten_san_pham' => 'Iphone 12 Pro 128gb',
                'slug_san_pham' => 'iphone-12-128gb',
                'so_luong' => 1,
                'don_gia_ban' => 30490000,
                'don_gia_khuyen_mai' => 26990000,
                'hinh_anh' => 'https://www.xtmobile.vn/vnt_upload/product/11_2020/thumbs/600_iphone_12_xanh_la_xtmobile.jpg',
                'mo_ta_ngan' => 'Nâng cao chất lượng ảnh với hệ thống camera được nâng cấp.
                                Chơi game 3D mượt mà với chip A15 Bionic, GPU 5 lõi mới.
                                Trải nghiệm quét 120 Hz, độ sáng 1200 nits.
                                Thao tác cảm ứng thông minh ProMotion và hệ điều hành iOS 15 mới',
                'mo_ta_chi_tiet' => 'Nâng cao chất lượng ảnh với hệ thống camera được nâng cấp.
                                    Chơi game 3D mượt mà với chip A15 Bionic, GPU 5 lõi mới.
                                    Trải nghiệm quét 120 Hz, độ sáng 1200 nits.
                                    Thao tác cảm ứng thông minh ProMotion và hệ điều hành iOS 15 mới',
                'danh_muc_id' => 3,
                'is_open' => 1,
            ],
        ]);
    }
}
