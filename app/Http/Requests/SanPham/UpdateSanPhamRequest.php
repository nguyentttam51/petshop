<?php

namespace App\Http\Requests\SanPham;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSanPhamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'             => 'required|exists:san_phams,id',
            'ten_san_pham'   => 'required|max:100',
            'slug_san_pham'  => 'required|unique:san_phams,slug_san_pham,' . $this->id,
            'is_open'        => 'required|boolean',
            'gia_ban'        => 'required|numeric|min:0',
            'gia_khuyen_mai' => 'nullable|lt:gia_ban',
            // 'so_luong'       => 'required|numeric|min:1',
            'danh_muc_id'    => 'required|exists:danh_mucs,id',
            'anh_dai_dien'   => 'required|',
            'mo_ta_ngan'     => 'required|max:500',
            'mo_ta_chi_tiet' => 'required',
        ];
    }
}
