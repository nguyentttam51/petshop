@jquery
@toastr_js
@toastr_render
<!-- latest jquery-->
<script src="/assets_admin/assets/js/jquery-3.5.1.min.js"></script>
<!-- Bootstrap js-->
<script src="/assets_admin/assets/js/bootstrap/bootstrap.bundle.min.js"></script>
<!-- feather icon js-->
<script src="/assets_admin/assets/js/icons/feather-icon/feather.min.js"></script>
<script src="/assets_admin/assets/js/icons/feather-icon/feather-icon.js"></script>
<!-- scrollbar js-->
<script src="/assets_admin/assets/js/scrollbar/simplebar.js"></script>
<script src="/assets_admin/assets/js/scrollbar/custom.js"></script>
<!-- Sidebar jquery-->
<script src="/assets_admin/assets/js/config.js"></script>
<!-- Plugins JS start-->
<script src="/assets_admin/assets/js/sidebar-menu.js"></script>
<script src="/assets_admin/assets/js/chart/chartist/chartist.js"></script>
<script src="/assets_admin/assets/js/chart/chartist/chartist-plugin-tooltip.js"></script>
<script src="/assets_admin/assets/js/chart/knob/knob.min.js"></script>
<script src="/assets_admin/assets/js/chart/knob/knob-chart.js"></script>
<script src="/assets_admin/assets/js/chart/apex-chart/apex-chart.js"></script>
<script src="/assets_admin/assets/js/chart/apex-chart/stock-prices.js"></script>
<script src="/assets_admin/assets/js/notify/bootstrap-notify.min.js"></script>
<script src="/assets_admin/assets/js/dashboard/default.js"></script>
<script src="/assets_admin/assets/js/notify/index.js"></script>
<script src="/assets_admin/assets/js/datepicker/date-picker/datepicker.js"></script>
<script src="/assets_admin/assets/js/datepicker/date-picker/datepicker.en.js"></script>
<script src="/assets_admin/assets/js/datepicker/date-picker/datepicker.custom.js"></script>
<script src="/assets_admin/assets/js/typeahead/handlebars.js"></script>
<script src="/assets_admin/assets/js/typeahead/typeahead.bundle.js"></script>
<script src="/assets_admin/assets/js/typeahead/typeahead.custom.js"></script>
<script src="/assets_admin/assets/js/typeahead-search/handlebars.js"></script>
<script src="/assets_admin/assets/js/typeahead-search/typeahead-custom.js"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="/assets_admin/assets/js/script.js"></script>
<script src="/assets_admin/assets/js/theme-customizer/customizer.js"></script>
<!-- login js-->
<!-- Plugin used-->
