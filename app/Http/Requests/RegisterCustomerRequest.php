<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterCustomerRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'full_name'     => 'required|min:5',
            'phone'         => 'required|digits:10',
            'email'         => 'required|email|unique:custommers,email',
            'sex'           => 'required|boolean',
            'dob'           => 'required|date',
            'password'      => 'required|min:6|max:30',
            're_password'   => 'required|same:password',
        ];
    }
}
