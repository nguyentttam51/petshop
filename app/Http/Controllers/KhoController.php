<?php

namespace App\Http\Controllers;

use App\Models\Kho;
use App\Models\SanPham;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
class KhoController extends Controller
{
    public function create()
    {
        return view('admin.page.kho.index');
    }

    public function store(Request $request)
    {
        foreach($request->send_data as $key => $value) {
            $san_pham = SanPham::find($value['id']);
            if(!$san_pham || $value['so_luong'] <= 0 || $value['don_gia'] < 1000) {
                return response()->json(['status' => false]);
            }
        }

        $hoa_don = Str::uuid();
        foreach($request->send_data as $key => $value) {
            $san_pham = SanPham::find($value['id']);
            Kho::create([
                'id_san_pham'    => $san_pham->id,
                'ten_san_pham'   => $san_pham->ten_san_pham,
                'so_luong_nhap'  => $value['so_luong'],
                'don_gia_nhap'   => $value['don_gia'],
                'hoa_don_nhap'   => $hoa_don,
            ]);
            $san_pham->so_luong += $value['so_luong'];
            $san_pham->save();
        }
        return response()->json(['status' => true]);
    }
}
