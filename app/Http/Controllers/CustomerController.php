<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterCustomerRequest;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class CustomerController extends Controller
{

    public function viewAuth()
    {
        return view('admin.page.login.login');
    }

    public function register(RegisterCustomerRequest $request)
    {
        $hash = Str::uuid()->toString();
        $data = $request->all();

        $data['hash']       = $hash;
        $data['password']   = bcrypt($request->password);
        $link = env('APP_URL') . '/kich-hoat/' . $hash;

        Customer::create($data);

        // Mail::to($request->email)->send(new CustomerActiveMail($request->full_name, $link));
        //to($request->email) gui toi email
        //($request->full_name, $link) can truyen 2 bien

        // sendMailActiveJob::dispatch($request->full_name,$link,$request->email);

        return response()->json(['status' => true]);
    }
    public function login(Request $request)
    {
        $data = $request->all();

        $login = Auth::guard('customer')->attempt($data);//tra ve true or false

        if($login){
            //da login roi co the xem thong tin tai khoan

            $user = Auth::guard('customer')->user(); //da login,lay thong tin tk

            if($user -> is_active ==1 ){
                return response()->json(['status' => 1]);
            }else{
                Auth::guard('customer')->logout();//ep logout -> chua login
                return response()->json(['status' => 2]);
            }
        }else{
            return response()->json(['status' => 0]);
        }
    }

}
