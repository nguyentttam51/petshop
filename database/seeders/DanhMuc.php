<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DanhMuc extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('danh_mucs')->delete();

        DB::table('danh_mucs')->truncate();

        DB::table('danh_mucs')->insert([
            // ['ma_danh_muc' => 'DT', 'ten_danh_muc' => 'Điện thoại', 'slug_danh_muc' => 'dien-thoai', 'id_danh_muc_cha' => 0, 'is_open' => 1, 'hinh_anh' => '/assets_client_new/images/dienthoai.png' ],
            // ['ma_danh_muc' => 'LAP', 'ten_danh_muc' => 'Laptop', 'slug_danh_muc' => 'laptop', 'id_danh_muc_cha' => 0, 'is_open' => 1, 'hinh_anh' => '/assets_client_new/images/laptop.png' ],
            // ['ma_danh_muc' => 'KEYB', 'ten_danh_muc' => 'Bàn phím', 'slug_danh_muc' => 'ban-phim', 'id_danh_muc_cha' => 0, 'is_open' => 1, 'hinh_anh' => '/assets_client_new/images/banphim.png' ],
            // ['ma_danh_muc' => 'WATCH', 'ten_danh_muc' => 'Đồng hồ', 'slug_danh_muc' => 'dong-ho', 'id_danh_muc_cha' => 0, 'is_open' => 1, 'hinh_anh' => 'https://cdn.tgdd.vn/Files/2021/02/02/1324942/aw6_1200x630.jpg' ],
           ]);
    }
}
