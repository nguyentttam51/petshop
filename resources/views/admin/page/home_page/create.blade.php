@extends('admin.master')
@section('title')
    Cấu hình trang chủ
    @endsection
@section('content')
<form action="/admin/homepage/create" method="post">
@csrf
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        @for($i = 1; $i < 4; $i++)
                        @php
                            $name = "slide_" . $i;
                            $name_link = "link_slide_" . $i;
                        @endphp
                        @if ($data)
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Hình ảnh Slide {{$i}}</label>
                                    <div class="input-group">
                                        <input id="slide_{{$i}}" class="form-control" name="slide_{{$i}}" value="{{$data->$name}}">
                                        <a data-input="slide_{{$i}}" data-preview="holder-icon_{{$i}}" class="lfm btn btn-success">
                                            Hình ảnh Slide {{$i}}
                                        </a>
                                    </div>
                                    <img id="holder-icon_{{$i}}" class="img-thumbnail" src="{{$data->$name}}" style="height: 210px">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Link Hình ảnh Slide {{$i}} </label>
                                    <input name="link_slide_{{$i}}" type="text" class="form-control" value="{{$data->$name_link}}">
                                </div>
                            </div>
                        </div>
                        @else
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Hình ảnh Slide {{$i}}</label>
                                    <div class="input-group">
                                        <input id="slide_{{$i}}" class="form-control" name="slide_{{$i}}" value="">
                                        <a data-input="slide_{{$i}}" data-preview="holder-icon_{{$i}}" class="lfm btn btn-success">
                                            Hình ảnh Slide {{$i}}
                                        </a>
                                    </div>
                                    <img id="holder-icon_{{$i}}" class="img-thumbnail" src="" style="height: 210px">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Link Hình ảnh Slide {{$i}} </label>
                                    <input name="link_slide_{{$i}}" type="text" class="form-control" value="">
                                </div>
                            </div>
                        </div>
                        @endif
                        @endfor
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        @for($i = 1; $i < 4; $i++)
                        @php
                            $name = "banner_" . $i;
                            $name_link = "banner_slide_" . $i;
                        @endphp
                        @if ($data)
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Hình ảnh banner {{$i}}</label>
                                    <div class="input-group">
                                        <input id="banner_{{$i}}" class="form-control" name="banner_{{$i}}" value="{{$data->$name}}">
                                        <a data-input="banner_{{$i}}" data-preview="holder-icon_banner_{{$i}}" class="lfm btn btn-success">
                                            Hình ảnh banner {{$i}}
                                        </a>
                                    </div>
                                    <img id="holder-icon_banner_{{$i}}" class="img-thumbnail" src="{{ $data->$name }}" style="height: 210px">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Link Hình ảnh banner {{$i}} </label>
                                    <input name="link_banner_{{$i}}" type="text" class="form-control" value="{{$data->$name_link}}">
                                </div>
                            </div>
                        </div>
                        @else
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Hình ảnh banner {{$i}}</label>
                                    <div class="input-group">
                                        <input id="banner_{{$i}}" class="form-control" name="banner_{{$i}}" value="">
                                        <a data-input="banner_{{$i}}" data-preview="holder-icon_banner_{{$i}}" class="lfm btn btn-success">
                                            Hình ảnh banner {{$i}}
                                        </a>
                                    </div>
                                    <img id="holder-icon_banner_{{$i}}" class="img-thumbnail" src="" style="height: 210px">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Link Hình ảnh banner {{$i}} </label>
                                    <input name="link_banner_{{$i}}" type="text" class="form-control" value="">
                                </div>
                            </div>
                        </div>
                        @endif

                        @endfor
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary btn-block waves-effect waves-float waves-light">
                    Cập Nhật Thông Tin Trang Chủ
                </button>
            </div>
        </div>
    </div>
</div>
</form>
@endsection
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
<script>
    $('.lfm').filemanager('slide_1');
    $('.lfm').filemanager('slide_2');
    $('.lfm').filemanager('slide_3');
    $('.lfm').filemanager('banner_1');
    $('.lfm').filemanager('banner_2');
    $('.lfm').filemanager('banner_3');
</script>
@endsection
