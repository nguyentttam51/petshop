@extends('admin.master')
@section('title')
    Quản Lý Sản Phẩm
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Tên Sản Phẩm</th>
                    <th scope="col">Hình Ảnh</th>
                    <th scope="col">Mô Tả Ngắn</th>
                    <th scope="col">Tình Trạng</th>
                    <th scope="col">Giá Bán</th>
                    <th scope="col">Giá Khuyến Mãi</th>
                    <th scope="col">Số Lượng</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($listSanPham as $key => $value)
                <tr>
                    <th scope="col">{{ $key + 1 }}</th>
                    <td class="text-nowrap">{{ $value->ten_san_pham }}</td>
                    <td><img src="{{ $value->anh_dai_dien }}" style="width: 200px; height: 200px;"></td>
                    <td class="text-justify">{{ Str::length($value->mo_ta_ngan) > 200 ? Str::substr($value->mo_ta_ngan, 0, 200) . '...' :  $value->mo_ta_ngan}} </td>
                    <td class="text-nowrap">{{ $value->is_open == 1 ? 'Đang kinh doanh' : 'Tạm dừng kinh doanh' }} </td>
                    <td>{{ number_format($value->gia_ban, 0, '.', ',') }} </td>
                    <td>{{ number_format($value->gia_khuyen_mai, 0, '.', ',') }} </td>
                    <td>{{ number_format($value->so_luong, 0, '.', ',') }} </td>
                    <td>
                        <a href="/admin/san-pham/edit/{{$value->id}}" class="btn btn-info btn-block text-nowrap mb-1">Edit</a>
                        <button class="btn btn-danger btn-block text-nowrap delete" data-id="{{$value->id}}" data-toggle="modal" data-target="#delete">Delete</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade text-left" id="delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Xóa Sản Phẩm</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="" id="idDanhMuc_delete">
                <h5>Bạn chắc chắn muốn xóa sản phẩm này!</h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                <button id="acceptDelete" type="button" class="btn btn-danger" data-dismiss="modal">Delete</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
    <script>
        $(document).ready(function(){
            var dong_can_xoa;
            $(".delete").click(function(){
                $("#idDanhMuc_delete").val($(this).data('id'));
                // console.log(123);
                dong_can_xoa = $(this).closest("tr");
            });
            $("#acceptDelete").click(function(){
                var id = $("#idDanhMuc_delete").val();

                $.ajax({
                    url     :   '/admin/san-pham/delete/' + id,
                    type    :   'get',
                    success :   function(res) {
                        if(res.status) {
                            toastr.success('Đã xóa sản phẩm thành công!');
                            dong_can_xoa.remove();
                            // location.reload(); 
                        } else {
                            toastr.error('Xoá sản phẩm thất bại!');
                        }
                    },
                });
            });
        });
    </script>
@endsection
