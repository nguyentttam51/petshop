@extends('admin.master')
@section('title')
    Cập nhật sản phẩm {{ $san_pham->ten_san_pham }}
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <input type="hidden" id="id" value="{{ $san_pham->id}} ">
                <div class="col-md-5">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Tên Sản Phẩm</label>
                            <input id="ten_san_pham" value="{{ $san_pham->ten_san_pham }}" type="text" class="form-control" placeholder="Nhập vào tên sản phẩm">
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Slug Sản Phẩm</label>
                            <input id="slug_san_pham" value="{{ $san_pham->slug_san_pham }}" type="text" class="form-control" placeholder="Nhập vào slug sản phẩm">
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Trạng Thái</label>
                            <select id="is_open" class="form-control">
                                <option value="1" {{ $san_pham->is_open == 1 ? 'selected' : ''}}>Hiển Thị</option>
                                <option value="0" {{ $san_pham->is_open == 0 ? 'selected' : ''}}>Tạm Tắt</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Giá bán</label>
                            <input id="gia_ban" value="{{ $san_pham->gia_ban }}" type="number" class="form-control" placeholder="Nhập vào giá bán">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Giá khuyến mãi</label>
                            <input id="gia_khuyen_mai" value="{{ $san_pham->gia_khuyen_mai }}" type="number" class="form-control" placeholder="Nhập vào giá khuyến mãi">
                        </div>
                    </div>
                </div>
                {{-- <div class="col-md-3">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Số lượng</label>
                            <input min="1" id="so_luong" value="{{ $san_pham->so_luong }}" type="number" class="form-control" placeholder="Nhập vào số lượng sản phẩm">
                        </div>
                    </div>
                </div> --}}
                <div class="col-md-4">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Danh mục sản phẩm</label>
                            <select id="danh_muc_id" class="form-control">
                                @foreach ($danh_muc as $value)
                                <option value="{{$value->id}}" {{ $san_pham->danh_muc_id == $value->id ? 'selected' : ''}}>{{$value->ten_danh_muc}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="col-md-12">
                        <div class="input-group">
                            <input value="{{ $san_pham->anh_dai_dien }}" id="anh_dai_dien" class="form-control" required>
                            <a data-input="anh_dai_dien" data-preview="holder-icon" class="lfm btn btn-success">
                              Up Ảnh
                            </a>
                        </div>
                        <img id="holder-icon" class="img-thumbnail" src="{{ $san_pham->anh_dai_dien }}" style="height: 210px">
                        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                        <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
                        <script>
                              $('.lfm').filemanager('anh_dai_dien');
                        </script>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Mô tả ngắn</label>
                            <textarea id="mo_ta_ngan" name="mo_ta_ngan" class="form-control" id="" rows="10" placeholder="Nhập vào mô tả ngắn">{{$san_pham->mo_ta_ngan}}</textarea>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Mô tả chi tiết</label>
                            <textarea id="mo_ta_chi_tiet" name="mo_ta_chi_tiet" class="form-control" id="" rows="15" placeholder="Nhập vào mô tả chi tiết">{{$san_pham->mo_ta_chi_tiet}}</textarea>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 mt-2">
                    <div class="row">
                        <div class="col-md-10">
                        </div>
                        <div class="col-md-2">
                            <button type="submit" id="updateProduct" class="btn btn-primary btn-block waves-effect waves-float waves-light">
                                Cập Nhật Sản Phẩm
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@section('js')
<script src="https://cdn.ckeditor.com/4.17.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('mo_ta_chi_tiet');
</script>
<script>
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    $(document).ready(function(e){
        $("#updateProduct").prop('disabled', true);

        function toSlug(str) {
            str = str.toLowerCase();
            str = str
                .normalize('NFD')
                .replace(/[\u0300-\u036f]/g, '');
            str = str.replace(/[đĐ]/g, 'd');
            str = str.replace(/([^0-9a-z-\s])/g, '');
            str = str.replace(/(\s+)/g, '-');
            str = str.replace(/-+/g, '-');
            str = str.replace(/^-+|-+$/g, '');
            return str;
        }

        function checkName(slug) {
            var payload = {
                'slug_san_pham'  : slug,
                'id'             : $("#id").val(),
            };
            $.ajax({
                url     :   '/admin/san-pham/check-slug',
                type    :   'post',
                data    :   payload,
                success :   function(res) {
                    if(res.status) {
                        toastr.error('Sản phẩm đã tồn tại trong hệ thống');
                        $("#updateProduct").prop('disabled', true);
                    } else {
                        toastr.success('Sản phẩm hợp lệ');
                        $("#updateProduct").prop('disabled', false);
                    }
                },
            });
        }

        $("#ten_san_pham").keyup(function(){
            var name = $(this).val(); // $("#ten_san_pham").val();
            $("#slug_san_pham").val(toSlug(name));
        });

        $("#ten_san_pham").blur(function(){
            var name = $(this).val();
            var slug = toSlug(name);
            checkName(slug);
        });

        $("#slug_san_pham").blur(function(){
            checkName($(this).val());
        });

        $("#updateProduct").click(function(e){
            var payload = {
                'id'             : $("#id").val(),
                'ten_san_pham'   : $("#ten_san_pham").val(),
                'slug_san_pham'  : $("#slug_san_pham").val(),
                'is_open'        : $("#is_open").val(),
                'gia_ban'        : $("#gia_ban").val(),
                'gia_khuyen_mai' : $("#gia_khuyen_mai").val(),
                'so_luong'       : $("#so_luong").val(),
                'danh_muc_id'    : $("#danh_muc_id").val(),
                'anh_dai_dien'   : $("#anh_dai_dien").val(),
                'mo_ta_ngan'     : $("#mo_ta_ngan").val(),
                'mo_ta_chi_tiet' : CKEDITOR.instances["mo_ta_chi_tiet"].getData(),
            };

            $.ajax({
                url     :   '/admin/san-pham/update',
                type    :   'post',
                data    :   payload,
                success :   function(res) {
                    if(res.status) {
                        toastr.success('Đã cập nhật sản phẩm thành công!');
                        window.location.href = "/admin/san-pham/index";
                    }
                },
                error   :   function(res) {
                    var errors = res.responseJSON.errors;
                    $.each(errors, function(k, v) {
                        toastr.error(v);
                    });
                }
            });
        });

    });
</script>
@endsection
