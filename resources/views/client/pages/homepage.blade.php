@extends('client.master')
@section('main_title')
    Home Page
@endsection
@section('sub_title')
    Welcome to Website
@endsection
@section('content')
    <!-- Header -->

    @include('client.components.title')

    <!-- Top columns -->

    @include('client.components.top_column')

    <!-- Sport collections -->

    @include('client.components.sport_collection')

    <hr class="m-0">

    <!-- Popular categories -->

    @include('client.components.popular_categories')

    <hr class="m-0">

    <!-- Sale black friday -->

    @include('client.components.black_friday')

    <!-- Banner winter is beautiful -->

    @include('client.components.banner_winter')

    <!-- Popular woman&#x27;s products -->

    @include('client.components.popular_woman')

    <!-- Footer banner -->

    @include('client.components.footer_banner')

    <!-- Clothing categories -->

    @include('client.components.clothing_categories')
@endsection
