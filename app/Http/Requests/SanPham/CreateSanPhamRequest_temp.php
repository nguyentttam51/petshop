<?php

namespace App\Http\Requests\SanPham;

use Illuminate\Foundation\Http\FormRequest;

class CreateSanPhamRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'ten_san_pham'   => 'required|max:100',
            'slug_san_pham'  => 'required|unique:san_phams,slug_san_pham',
            'is_open'        => 'required|boolean',
            'gia_ban'        => 'required|numeric|min:0',
            'gia_khuyen_mai' => 'nullable|lt:gia_ban',
            // 'so_luong'       => 'required|numeric|min:1',
            'danh_muc_id'    => 'required|exists:danh_mucs,id',
            'anh_dai_dien'   => 'required|',
            'mo_ta_ngan'     => 'required|max:500',
            'mo_ta_chi_tiet' => 'required',
        ];
    }
}
