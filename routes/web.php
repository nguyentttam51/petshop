<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('client.master');
});
Route::get('/product/{id}', [\App\Http\Controllers\HomepageController::class, 'detailProduct']);
Route::get('/category/{id}', [\App\Http\Controllers\HomepageController::class, 'listProduct']);
// Route::get('/login', [\App\Http\Controllers\CustomerController::class, 'viewAuth']);
// Route::post('/register', [\App\Http\Controllers\CustomerController::class, 'register']);
// Route::post('/login', [\App\Http\Controllers\CustomerController::class, 'login']);
// Route::get('/kich-hoat/{hash}', [\App\Http\Controllers\CustomerController::class, 'active']);

Route::group(['prefix' => '/admin'], function() {

    Route::group(['prefix' => '/danh-muc'], function() {
        Route::get('/index', [\App\Http\Controllers\DanhMucController::class, 'index']);

        Route::get('/get-data', [\App\Http\Controllers\DanhMucController::class, 'getData']);
        Route::get('/update-status/{id}',  [\App\Http\Controllers\DanhMucController::class, 'updateStatus']);

        Route::post('/index', [\App\Http\Controllers\DanhMucController::class, 'store']);
        Route::get('/edit/{id}', [\App\Http\Controllers\DanhMucController::class, 'edit']);
        Route::post('/update', [\App\Http\Controllers\DanhMucController::class, 'update']);

        Route::get('/destroy/{id}', [\App\Http\Controllers\DanhMucController::class, 'destroy']);
    });
    Route::group(['prefix' => '/san-pham'], function(){
        Route::get('/index', [\App\Http\Controllers\SanPhamController::class, 'index']);

        Route::get('/create', [\App\Http\Controllers\SanPhamController::class, 'create']);
        Route::post('/create', [\App\Http\Controllers\SanPhamController::class, 'store']);
        Route::post('/check-slug', [\App\Http\Controllers\SanPhamController::class, 'checkSlug']);

        Route::get('/edit/{id}', [\App\Http\Controllers\SanPhamController::class, 'edit']);
        Route::post('/update', [\App\Http\Controllers\SanPhamController::class, 'update']);

        Route::get('/delete/{id}', [\App\Http\Controllers\SanPhamController::class, 'destroy']);
    });
    Route::group(['prefix' => '/nhap-kho'], function(){
        Route::get('/create', [\App\Http\Controllers\KhoController::class, 'create']);
        Route::post('/create',[\App\Http\Controllers\KhoController::class, 'store']);

        Route::get('/list-san-pham', [\App\Http\Controllers\SanPhamController::class, 'list']);
        Route::post('/search', [\App\Http\Controllers\SanPhamController::class, 'search']);
    });
    // Route::group(['prefix' => '/client', 'middleware' => 'ClientMiddleware'], function() {
    //     Route::get('/add-to-cart/{id_san_pham}', [\App\Http\Controllers\ChiTietDonHangController::class, 'store']);
    //     Route::get('/cart', [\App\Http\Controllers\ChiTietDonHangController::class, 'index']);
    //     Route::get('/cart/data', [\App\Http\Controllers\ChiTietDonHangController::class, 'dataCart']);
    //     Route::get('/cart/remove/{id}', [\App\Http\Controllers\ChiTietDonHangController::class, 'removeCart']);
    //     Route::post('/cart/update', [\App\Http\Controllers\ChiTietDonHangController::class, 'update']);

    //     Route::post('/bill/create', [\App\Http\Controllers\BillController::class, 'store']);
    //     Route::get('/bill-order', [\App\Http\Controllers\BillController::class, 'index']);
    //     Route::get('/bill/order', [\App\Http\Controllers\BillController::class, 'listOrder']);
    //     Route::get('/all-bill', [\App\Http\Controllers\BillController::class, 'listBill']);
    // });
    Route::group(['prefix' => '/homepage'], function(){
        Route::get('/create', [\App\Http\Controllers\HomepageController::class, 'create']);
        Route::post('/create', [\App\Http\Controllers\HomepageController::class, 'store']);
    });
});


